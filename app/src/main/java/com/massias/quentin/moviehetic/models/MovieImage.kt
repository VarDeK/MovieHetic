package com.massias.quentin.moviehetic.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieImage(@SerializedName("thumb_url") val thumbUrl: String) : Parcelable