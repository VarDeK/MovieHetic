package com.massias.quentin.moviehetic.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Movie(val image: MovieImage, val name: String) : Parcelable