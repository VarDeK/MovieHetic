package com.massias.quentin.moviehetic.constants

object ComicVineUri {
    const val BASE_URI = "https://comicvine.gamespot.com/api/"

    const val URI_MOVIE_LIST = "movies/" //Retrieves movie list
}