package com.massias.quentin.moviehetic

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import com.massias.quentin.moviehetic.adapters.MovieAdapter
import com.massias.quentin.moviehetic.api.ComicVineApi
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*
import android.support.v7.widget.DividerItemDecoration



class MainActivity : AppCompatActivity() {

    private var disposable: Disposable? = null

    private val comicVineApi: ComicVineApi by lazy {
        ComicVineApi(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // TODO: searchBarEditText.addTextChangedListener(...)

        displayMovies()
    }

    private fun displayMovies() {
        val options = HashMap<String, String>()
        options["format"] = "json"

        disposable = comicVineApi.movieService.getMovies(options)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    result ->
                        // Creates a vertical Layout Manager
                        movieRecyclerView.layoutManager = LinearLayoutManager(this)

                        // Access the RecyclerView Adapter and load the data into it
                        movieRecyclerView.adapter = MovieAdapter(result.results, this)

                        // Add separator between each item in RecyclerView
                        val dividerItemDecoration = DividerItemDecoration(movieRecyclerView.context, LinearLayoutManager(this).orientation)
                        movieRecyclerView.addItemDecoration(dividerItemDecoration)
                },
                {
                    error ->
                        // Display the error message if something wrong happened
                        Toast.makeText(this, error.message, Toast.LENGTH_SHORT).show()
                }
            )
    }
}
