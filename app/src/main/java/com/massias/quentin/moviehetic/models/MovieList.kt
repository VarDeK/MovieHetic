package com.massias.quentin.moviehetic.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MovieList(val results: ArrayList<Movie>) : Parcelable