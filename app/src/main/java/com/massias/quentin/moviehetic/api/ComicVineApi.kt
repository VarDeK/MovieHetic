package com.massias.quentin.moviehetic.api

import android.content.Context
import com.massias.quentin.moviehetic.R
import com.massias.quentin.moviehetic.constants.ComicVineUri
import com.massias.quentin.moviehetic.services.MovieService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Creates and configures a REST adapter for ComicVine API.
 *
 * Basic usage:
 * ComicVineApi comicVineApi = new ComicVineApi(endPoint);
 *
 * Create instance of ComicVineApi with given executors.
 * @property callbackExecutor executor for callbacks. If null is passed than the same
 *
 * @property movieService instance for access to movies data
 * @constructor thread that created the instances.
 */
class ComicVineApi(callbackExecutor: Executor, var context: Context? = null)
{
    val movieService: MovieService

    init {
        val okHttpClient = OkHttpClient.Builder().addInterceptor(AddHeaderTokenInterceptor())
        val restAdapterBuilder = Retrofit.Builder()
            .baseUrl(ComicVineUri.BASE_URI)
            .callbackExecutor(callbackExecutor)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient.build())

        val restAdapter = restAdapterBuilder.build()

        movieService = restAdapter.create(MovieService::class.java)
    }

    /**
     *  New instance of ComicVineApi,
     *  with single thread executor both for http and callbacks.
     */
    constructor(context: Context?) : this(Executors.newSingleThreadExecutor(), context)

    /**
     * The request interceptor that will add the API key
     * to every request made with the wrapper.
     */
    inner class AddHeaderTokenInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): Response {
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", context?.getString(R.string.comicVineApiKey))
                .build()

            // Request customization: add request headers
            val requestBuilder = original.newBuilder()
                .url(url)

            val request = requestBuilder.build()
            return chain.proceed(request)
        }
    }

}