package com.massias.quentin.moviehetic.services

import com.massias.quentin.moviehetic.constants.ComicVineUri
import com.massias.quentin.moviehetic.models.MovieList
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface MovieService {
    /**
     * Retrieve all movies.
     *
     * @param options Optional parameters. Nullable
     * @return [Observable] requested all movies
     * @see <a href="https://comicvine.gamespot.com/api/documentation#toc-0-15">Retrieve all movies</a>
     */
    @GET(ComicVineUri.URI_MOVIE_LIST)
    fun getMovies(@QueryMap options: Map<String, String>? = mapOf()): Observable<MovieList>
}