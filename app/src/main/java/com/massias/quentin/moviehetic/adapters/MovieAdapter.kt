package com.massias.quentin.moviehetic.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.massias.quentin.moviehetic.R
import com.massias.quentin.moviehetic.models.Movie
import kotlinx.android.synthetic.main.movie_list_item.view.*

class MovieAdapter(private val items : ArrayList<Movie>, private val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of movies in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.movie_list_item, parent, false))
    }

    // Binds each movie in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.movieName.text = items[position].name
        Glide
            .with(context)
            .load(items[position].image.thumbUrl)
            .into(holder.movieImage)
    }
}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView and ImageView that will add each movie to
    val movieName: TextView = view.movieNameTextView
    val movieImage: ImageView = view.movieImageView
}